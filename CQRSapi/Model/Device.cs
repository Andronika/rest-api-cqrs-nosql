﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DSACopy.Model
{
    public class Device
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
        public DeviceTypes Type { get; set; }
        public string Location { get; set; }
    }
}
