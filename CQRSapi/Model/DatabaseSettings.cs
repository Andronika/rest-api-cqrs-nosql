﻿namespace DSACopy.Model
{
    public class DatabaseSettings : IDatabaseSettings
    {
        public string DeviceTypesCollections { get ; set; }
        public string DeviceCollections { get ; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        
    }
    public interface IDatabaseSettings
    {
        public string DeviceTypesCollections { get; set; }
        public string DeviceCollections { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
