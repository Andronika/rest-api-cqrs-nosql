﻿using CSharpFunctionalExtensions;
using DSACopy.Commands;
using DSACopy.Model;
using DSACopy.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace DSACopy.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TypesController : ControllerBase
    {
        private readonly DeviceTypesServices _deviceTypesServices;
        private readonly Messages _messages;

        public TypesController(DeviceTypesServices deviceTypeServices, Messages messages)
        {
            _deviceTypesServices = deviceTypeServices;
            _messages = messages;
        }

        [HttpGet]
        public ActionResult<List<DeviceTypes>> Get()
        {
            return Ok(_deviceTypesServices.Get());
        }
        [HttpGet("{id:length(24)}", Name = "GetDeviceType")]
        public ActionResult<DeviceTypes> Get(string id)
        {
            var type = _deviceTypesServices.Get(id);
            if (type == null)
                return NoContent();

            return Ok(type);
        }

        [HttpPost]
        public ActionResult<DeviceTypes> Create(DeviceTypes type)
        {
            _deviceTypesServices.Create(type);

            return CreatedAtRoute("GetDeviceType", new { id = type.Id }, type);
        }

        [HttpPut]
        public IActionResult Put([FromBody] EditDeviceTypeCommad command)
        {
            Result result = _messages.Dispatch(command);
            return result.IsSuccess ? Ok() : (IActionResult)BadRequest();
        }

        [HttpDelete("{id:length(24)}")]
        public ActionResult Delete(string id)
        {
            var type = _deviceTypesServices.Get(id);
            if (type == null)
                return NotFound();

            _deviceTypesServices.Remove(id);
            return NoContent();
        }
    }
}