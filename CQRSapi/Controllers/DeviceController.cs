﻿using CSharpFunctionalExtensions;
using DSACopy.Commands;
using DSACopy.Model;
using DSACopy.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace DSACopy.Controllers
{
    [Route("api/Device")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        private readonly DeviceService _deviceService;
        private readonly Messages _messages;

        public DeviceController(DeviceService deviceService, Messages messages)
        {
            _deviceService = deviceService;
            _messages = messages;
        }

        [HttpGet(Name = "GetDevices")]
        [Route("/{orderBy?}/{sortOrder?}/{Location}")]
        public ActionResult<List<Device>> Get([FromQuery]string orderBy, [FromQuery]string sortOrder, [FromQuery] string[] Location )
        {
            return _deviceService.Get(orderBy, sortOrder, Location);
        }

        [HttpPost]
        public IActionResult Post(CreateDeviceCommand request)
        {
            Result result = _messages.Dispatch(request);
            return Ok();
        }
    }
}