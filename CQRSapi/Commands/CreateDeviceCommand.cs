﻿using CSharpFunctionalExtensions;
using DSACopy.Commands;
using DSACopy.Model;
using DSACopy.Services;

namespace DSACopy.Commands
{
    public sealed class CreateDeviceCommand : ICommand
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DeviceTypes Type { get; set; }
        public string Location { get; set; }
    }

    public class CreateDeviceCommandHandler : ICommandHander<CreateDeviceCommand>
    {
        private readonly DeviceService _deviceService;

        public CreateDeviceCommandHandler(DeviceService deviceService)
        {
            _deviceService = deviceService;
        }
        public Result Handle(CreateDeviceCommand request)
        {
            Device device = new Device();
            device.Id = request.Id;
            device.Name = request.Name;
            device.Location = request.Location;
            device.Type = request.Type;
            _deviceService.Create(device);
            return Result.Success();
        }
    }
}
