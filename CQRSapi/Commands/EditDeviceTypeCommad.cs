﻿using CSharpFunctionalExtensions;
using DSACopy.Model;
using DSACopy.Services;

namespace DSACopy.Commands
{
    public interface ICommand
    {

    }
    public sealed class EditDeviceTypeCommad : ICommand
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
    }
    public interface ICommandHander<TCommand> where TCommand : ICommand
    {
        Result Handle(TCommand command);
    }
    public sealed class EditDeviceTypeCommandHandler : ICommandHander<EditDeviceTypeCommad>
    {
        private readonly DeviceTypesServices _deviceTypesServices;
        public EditDeviceTypeCommandHandler(DeviceTypesServices deviceTypesServices)
        {
            _deviceTypesServices = deviceTypesServices;
        }
        public Result Handle(EditDeviceTypeCommad command)
        {
            var deviceType = _deviceTypesServices.Get(command.Id);
            if (deviceType == null)
                return Result.Failure($"No such deviceType {command.Id}");

            deviceType.Name = command.Name;
            deviceType.Color = command.Color;

            _deviceTypesServices.Update(command.Id, deviceType);
            return Result.Success();
        }
    }

}
