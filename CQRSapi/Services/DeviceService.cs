﻿using DSACopy.Model;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;

namespace DSACopy.Services
{
    public class DeviceService : IDeviceService
    {
        public Dictionary<string, int> sorting;

        private readonly IMongoCollection<Device> _device;
        public DeviceService(IDatabaseSettings settings )
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _device = database.GetCollection<Device>(settings.DeviceCollections);
            sorting = new Dictionary<string, int>
            {
                { "Descending", -1 },
                { "Ascending" , 1}
            };
        }

        public void Create(Device device)
        {
            _device.InsertOne(device);
        }
        public List<Device> Get(string orderBy = "", 
                                string sortOrder = "",
                                string[] location = null)
        {

            var filterBuilder = Builders<Device>.Filter;
            var filter = filterBuilder.Empty;

            filter = FiltrByLocation(location, filter, filterBuilder);
            
            if (string.IsNullOrEmpty(orderBy) || string.IsNullOrEmpty(sortOrder))
                return _device.Find((location.Length == 0) ? (Builders<Device>.Filter.Empty) : filter).ToList();

            var sortingOrder = FindSortOrder(sortOrder);

            return _device.Find((location.Length == 0) ? (Builders<Device>.Filter.Empty) : filter).
                Sort($"{{{orderBy} : {sortingOrder}}}").ToList();

        }

        private int FindSortOrder(string sortOrder)
        {
            var order = sorting.FirstOrDefault(s => s.Key == sortOrder).Value;
            if (order != 0)
                return order;

            return sorting["Ascending"];
        }

        private FilterDefinition<Device> FiltrByLocation(string[] location, FilterDefinition<Device> filter, FilterDefinitionBuilder<Device> filterBuilder)
        {
            if (location.Length != 0)
            {
                for (int i = 0; i < location.Length; i++)
                {
                    var nextFilterCondition = filterBuilder.Eq("Location", location[i]);
                    if (filter.Equals(FilterDefinition<Device>.Empty))
                    {
                        filter = nextFilterCondition;
                    }
                    else
                    {
                        filter = filter | nextFilterCondition;
                    }
                }
            }
            return filter;
        }
    }
}
