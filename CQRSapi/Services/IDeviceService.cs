﻿using DSACopy.Model;

namespace DSACopy.Services
{
    interface IDeviceService
    {
        void Create(Device device);
    }
}
