﻿using DSACopy.Model;
using System.Collections.Generic;

namespace CQRSapi.Services
{
    interface IDeviceTypesService
    {
        public List<DeviceTypes> Get();
        public DeviceTypes Get(string id);
        public void Update(string id, DeviceTypes deviceTypes);
        public void Remove(string id);
        public void Create(DeviceTypes deviceTypes);
    }
}
