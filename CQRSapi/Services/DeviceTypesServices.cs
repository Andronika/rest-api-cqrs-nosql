﻿using CQRSapi.Services;
using DSACopy.Model;
using MongoDB.Driver;
using System.Collections.Generic;

namespace DSACopy.Services
{
    public class DeviceTypesServices : IDeviceTypesService
    {
        private readonly IMongoCollection<DeviceTypes> _deviceTypes;
        public DeviceTypesServices(IDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _deviceTypes = database.GetCollection<DeviceTypes>(settings.DeviceTypesCollections);
        }

        public List<DeviceTypes> Get()
        {
            return _deviceTypes.Find(type => true).ToList();
        }
        public DeviceTypes Get(string id)
        {
            return _deviceTypes.Find(type => type.Id == id).FirstOrDefault();
        }
        public void Update(string id, DeviceTypes deviceTypes)
        {
            _deviceTypes.ReplaceOne(type => type.Id == id, deviceTypes);
        }
        public void Remove(string id)
        {
            _deviceTypes.DeleteOne(type => type.Id == id);
        }
        public void Create(DeviceTypes deviceTypes)
        {
            _deviceTypes.InsertOne(deviceTypes);
        }
    }
}
